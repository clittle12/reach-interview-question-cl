from django.db import models


#This class represents the name of the guest
class GuestName(models.Model):
	name = models.CharField(max_length=25, blank=False, unique=True)
	
	#Return readable version of the instance
	def __str__(self):
		return "{}".format(self.name)