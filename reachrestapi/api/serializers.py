from rest_framework import serializers
from .models import GuestName

#Maps the model instance into JSON format
class GuestNameSerializer(serializers.ModelSerializer):
	
	#Maps the serializer fields with model fields
	class Meta:
		model = GuestName
		fields = ('id', 'name')