from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import GuestNameSerializer
from .models import GuestName

# function based view instead of a class based view
# decorator required to create the REST api and provide the proper methods for it
@api_view(['GET', 'POST', 'DELETE']) 
def greeting(request):

	objs = GuestName.objects.count()
	
	# gets name of the guest, if there isn't a guess the exception is passed
	try:
		name = GuestName.objects.get()
	except:
		pass
	
	# If GET request and there are objects in the DB then greet the name, otherwise greet the guest
	if request.method == 'GET':
		if objs:
			return Response({"Hello, {0}".format(name)})
		return Response({"Hello, Guest"})
	
	# If POST request and there are no objects in the DB then serialize and save the data, otherwise show http 409 error
	elif request.method == 'POST':
		if objs < 1:		
			serializer = GuestNameSerializer(data=request.data)
			if serializer.is_valid():
				serializer.save()
				return Response(serializer.data, status=status.HTTP_201_CREATED)# display json object of saved name
		return Response("Guest name already present in the system, delete it first", status=status.HTTP_409_CONFLICT)
	
	# If DELETE request and there is a name in the system, delete it
	# otherwise display http code that isnt 500 internal server error (basically do nothing and appear to ignore it)
	elif request.method == 'DELETE':
		if objs > 0:
			name.delete()
			return Response(status=status.HTTP_204_NO_CONTENT)
		return Response(status=status.HTTP_204_NO_CONTENT)
		

	
	