from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from api import views

urlpatterns = {
	url(r'^greeting/$', views.greeting, name="greeting"),
}

urlpatterns = format_suffix_patterns(urlpatterns)